<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Crawl;
use App\Models\Link;
use App\Models\Page;
use App\Http\Controllers\PageController;
use Illuminate\Support\Collection;
use App\Models\Verify;

class CrawlController extends Controller
{


    // var $rounds specifes how many pages will be crawled
    // var $url specifies the initial url
    
    public function startCrawl(Request $request)
    {
        // params defaults:
        $scheme     = isset($request->scheme) ? $request->scheme : 'https';
        $url        = isset($request->url) ? $request->url : 'agencyanalytics.com';
        $rounds     = isset($request->rounds) ? $request->rounds : 6;

        $verify = new Verify;

        $verify->initData($scheme,$url,$rounds); //simple data verification

        $crawl = new Crawl;

        $total_load_time    = 0;
        $total_word_count   = 0;
        $total_title_length = 0;
        $not_crawled_url    = 0;

        $total_link_qty = [
            'external'  =>  0,
            'internal'  =>  0,
        ];

        // this will save all images found in crawled pages
        $img_array = array();

        // this list will save each links for every page as an index to control progress
        $total_crawled_list     =   array();    // only indexing and fast searching purposes
        $total_crawled_type     =   array();    // data purposes


        for($i=0 ; $i < $rounds ; $i++){
            
            if (!isset($total_crawled_list[0])) {
                // $skip = false;
                // creates the string with the full url to crawl
                $base_url               =   $scheme . '://' . $url;
                $full_url               =   $scheme . '://' . $url;
                $total_crawled_list[]   =   $full_url;               
            
            }else{

                if(isset($total_crawled_list[$i])){

                    $full_url   =   $total_crawled_list[$i];
                
                }else{
                
                    $skip = true;
                
                }
            }
            if (!isset($skip)){
                
                $control = 0;
                do{
                    $page                   =   new Page;
                    $retrieve               =   $page->crawlMe($total_crawled_list,$total_crawled_type,$url,$scheme,$full_url,$total_link_qty,$img_array);
                    
                        $total_crawled_list     =   $retrieve['total_crawled_list'] ?? ($total_crawled_list ?? []);
                        $total_crawled_type     =   $retrieve['total_crawled_type'] ?? ($total_crawled_type ?? []);
                        $page_crawled_list[]    =   $retrieve['page_crawled_list'] ?? [];
                        $total_link_qty         =   $retrieve['total_link_qty'] ?? $total_link_qty;
                        $img_array              =   $retrieve['img_array'] ?? $img_array;

                    $control++;
                    if (isset($retrieve['skip'])){
                        ++$i;
                        ++$rounds;
                        ++$not_crawled_url;
                        $full_url   =   $total_crawled_list[$i];
                    }
                    if ($control > 10) break; // top to avoid infinite loop
                }while(isset($retrieve['skip']));   

            }

        }

        // accumulating to calculate averages
        foreach ($page_crawled_list as $key =>$pageCrawl) {
            foreach ($pageCrawl as $pageInfo){          
                    $total_load_time    = !isset($total_load_time)  ? ($pageInfo['load_time'] ?? 0) : $total_load_time + ($pageInfo['load_time'] ?? 0);
                    $total_word_count   = !isset($total_word_count) ? ($pageInfo['word_count'] ?? 0) : $total_word_count + ($pageInfo['word_count'] ?? 0) ;
                    $total_title_length = !isset($total_title_length) ? strlen(($pageInfo['title'] ?? "")) : $total_title_length + strlen(($pageInfo['title'] ?? ""));
                    $crawled[] = array (
                        'url'                   =>  $pageInfo['url'],
                        'response'              =>  $pageInfo['response'],
                        'crawled'               =>  $pageInfo['crawled'],
                    );
            }
        }

        // clean duplicated and empty images from array (only bringing img/src from crawling)
        $img_array = array_diff(array_unique($img_array),array(""));

        // average calculation
        $average = [
            'load_time'     => round($total_load_time / $rounds, 3),
            'word_count'    => round($total_word_count / $rounds,1),
            'title_length'  => round($total_title_length / $rounds,1),
        ];

        $pages_crawled = $rounds - $not_crawled_url;
        $unique_images = count($img_array);
        $unique_external_links = $total_link_qty['external'] ?? 0;
        $unique_internal_links = $total_link_qty['internal'] ?? 0;

        return compact('base_url','pages_crawled','unique_images','unique_internal_links','unique_external_links','average','crawled');

    }

}
