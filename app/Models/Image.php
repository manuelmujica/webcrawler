<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $attributes = [
        'url'       => '',  // main link
    ];

    public function isCrawledTotal($url,$total_crawl_img)
    {
        // new normalized url
        $link = $this->url;

		if (!in_array($link, $total_crawl_img)) {
            return $link;
        }

        return false;

    }

}
