<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Verify extends Model
{
    use HasFactory;

    public function initData($scheme,$url,$rounds)
    {
        
        $scheme_ok      =   in_array($scheme , ['http','https']);
        $url_ok         =   is_string($url);
        $rounds_ok      =   $rounds < 100;
        
        // simple url parameter validation
        $ok = $scheme_ok && $url_ok && $rounds_ok;

        if ($ok) return true;
        return false;
    }
}
