<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\Handler;
use Exception;

class Page extends Model
{
    use HasFactory;

    protected $attributes = [
        'title'         => '',
        'page_load'     => '',
        'word_count'    => '',
        'title'         => '',
        'response'      => '',
    ];

    public function getLinks()
    {
        $this->hasMany(App\Models\Link::class);
    }

    // var $url specifies the address to crawl
    public function crawlMe ($total_crawled_list,$total_crawled_type,$url,$scheme,$full_url,$total_link_qty,$img_array){
        
            $external_link_qty          = 0;
            $internal_link_qty          = 0;
            $total_external_link_qty    = 0;
            $total_internal_link_qty    = 0;
            $page_crawled_list = array();

            // php class to handle html documents
            $document = new \DOMDocument();
    
            // start timing just before downloading customer's website
            $starttime = microtime(true); // Top of page
            
                try {

                    $download   = file_get_contents($full_url);
                    $http_status = isset($http_response_header[0]) ? $http_response_header[0] :'-';

                }catch(Exception $e){

                    $http_status = isset($http_response_header[0]) ? $http_response_header[0] :'-';

                    $page_crawled_list[] = array(
                        'url'                   =>  $full_url,
                        'response'              =>  $http_status,
                        'crawled'               =>  false,
                    );
                    
                    $skip = true;
                    return compact('skip','page_crawled_list');                    
                
                }
        
                /************************************************/
                //remove warnings caused by not html characters
                $internalErrors = libxml_use_internal_errors(true);
                // parse webpage
                $document->loadHTML($download);
                // restore warnings caused by not html characters
                libxml_use_internal_errors($internalErrors);
                /************************************************/
                
                // end timing just before downloading customer's website
                $endtime = microtime(true); // Bottom of page
        
                // adding the starting url so it doesn't repeat anymore
        
                $this->response =    $http_status;
        
                $internal_links =   $document->getElementsByTagName("a");       // brings links
                
                $title          =   $document->getElementsByTagName("title");   // brings titles
        
                $images         =   $document->getElementsByTagName("img");     // brings images
                
                foreach ($images as $image) {
                    $img_array_page[] = $image->getAttribute('src');
                }

                // return this merged array to controller
                $img_array = array_merge($img_array,($img_array_page ?? []));
          
                // handles error when page doesn't have any title
                try{
                    $this->title    =   $title->item(0)->nodeValue;
                }catch (Exception $e){
                    $this->title    =  'title-error';
                }
        
                $page_crawled_list_links     = array();
        
        
                foreach ($internal_links as $link) {
        
                    $web_link           = new Link;
                    $web_link->url      = $link->getAttribute("href");
                    
                    $web_link->normalizeUrl($url,$scheme);
        
                    $total_link_info = $web_link->isCrawledTotal($web_link->url,$total_crawled_list);
                    
                    $web_link->type = $web_link->isExternal($web_link->url,$url);
        
                    // total (global) links
                    if ($total_link_info) {
                        $total_crawled_list[] = $total_link_info;                   // fast indexing purposes
                        $total_crawled_type[] = [                                   // data purposes
                            'link' => $total_link_info,                             
                            'type' => $web_link->type,                              
                        ];
                        if ($web_link->type ==  'external') $total_external_link_qty++;
                        else $total_internal_link_qty++;
                    }
        
                    // page links
                    $page_link_info = $web_link->isCrawledPage($web_link->url,$page_crawled_list_links);
                    if ($page_link_info){
                        $page_crawled_list_links[] = $page_link_info;       // fast indexing purposes
                        $page_crawled_list_types[] = [                      // data purposes
                            'link'  => $page_link_info,
                            'type'  => $web_link->type,
                        ];
                        if ($web_link->type ==  'external') $external_link_qty++;
                        else $internal_link_qty++;
                    }
        
                }

                $page_link = new Link;
                $page_link->isExternal($full_url,$url);

                $words = $this->countWords($document);

                $page_crawled_list[] = array(
                    'url'                   =>  $full_url,
                    'title'                 =>  $this->title,
                    'response'              =>  $this->response,
                    'crawled'               =>  true,
                    'load_time'             =>  round($endtime - $starttime,3),
                    'word_count'            =>  $words['count'],
                    'words'                 =>  $words['words'],
                    'internal_link_qty'     =>  $internal_link_qty ?? 0,
                    'external_link_qty'     =>  $external_link_qty ?? 0,
                    'links'                 =>  $page_crawled_list_types ?? [],

                );
        
                
                $total_link_qty = [
                    'external'  =>  $total_link_qty['external'] +  $total_external_link_qty,
                    'internal'  =>  $total_link_qty['internal'] +  $total_internal_link_qty,
                ];
        
        
                // returning unique links per crawl (total) and per page
        
                return compact('total_crawled_list','page_crawled_list','total_link_qty','img_array');
            
        
        }

        private function countWords($document)
        {

            // removes html tags
            $tags_to_remove = array('head','header','script','style','iframe','link','meta','noscript');

            foreach($tags_to_remove as $tag){
                $element = $document->getElementsByTagName($tag);
                foreach($element  as $item){
                    $item->parentNode->removeChild($item);
                }
            }

            // removes CDATA info
            $xpath = new \DOMXpath($document);

            foreach($xpath->evaluate('//text()') as $section) {
                if ($section instanceof \DOMCDATASection) {
                    $section->parentNode->removeChild($section);
                }
            }

            $words_array = [
                'words'     => str_word_count($document->textContent,1) ,
                'count'     => str_word_count($document->textContent,0) ,
            ];

            return $words_array;
        }

}
