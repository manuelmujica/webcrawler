<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use HasFactory;

    protected $attributes = [
        'url'       => '',  // main link
        'type'      => '',  // internal or external
        'base_url'  => '',  // base website where this link belongs
    ];

    public function normalizeUrl($url,$scheme)
    {

        /*
        *   This function will help normalize (sanitize) all links so they can be compared
        *   and classified correctly avoiding duplicated entries
        *   Ex. /link.hml and http://example.com/link.html
        */

        $newUrl = $this->url;

        // String process for each link found

            // relative paths (ex. /example.com)
		if (substr($newUrl, 0, 1) == "/" && substr($newUrl, 0, 2) != "//") {
			$newUrl = $scheme . "://".$url . $newUrl;
		}   // absolute paths (ex. //example.com)
        
        else if (substr($newUrl, 0, 2) == "//") {
			$newUrl = $scheme . ":".$newUrl;
		}   // current directory paths (ex. ./example.com) 
        
        else if (substr($newUrl, 0, 2) == "./") {
			$newUrl = $scheme . "://".$url . dirname(parse_url($url)["path"]).substr($newUrl, 1);
		}   // anchor paths (ex. example.com#footer) 
        
        else if (substr($newUrl, 0, 1) == "#") {
			$newUrl = $scheme . "://".$url . parse_url($url)["path"].$newUrl;
		}   // parent directory paths (ex. ../example.com) 
        
        else if (substr($newUrl, 0, 3) == "../") {
			$newUrl = $scheme . "://".$url . "/".$newUrl;
		} 
        
        else if (substr($newUrl, 0, 11) == "javascript:") {
			// javascripts functions in href field
		} 
        
        else if (substr($newUrl, 0, 5) != "https" && substr($newUrl, 0, 4) != "http") {
			$newUrl = $scheme . "://".$url . "/".$newUrl;
		}   
        
        // same path ending with and without slash 
        if (substr( $newUrl , -1 ) == "/" ) {
            $newURL = mb_substr($newUrl,0,-1);
        }

        // Saves the normalized string to the link url
        $this->url = $newUrl;

    }

    public function isCrawledTotal($url,$total_crawl_list)
    {
        // new normalized url
        $link = $this->url;

		if (!in_array($link, $total_crawl_list)) {
            return $link;
        }

        return false;

    }

    public function isCrawledPage($url,$page_crawl_list)
    {
        // new normalized url
        $link = $this->url;

		if (!in_array($link, $page_crawl_list)) {
            return $link;
        }

        return false;

    }

    public function isExternal($link , $url)
    {
            //  $link : current evaluated link
            //  $url : base url
         
            if (strpos($link,'www.'.$url) > -1 || strpos($link,'/'.$url) > -1 || preg_match('/\/[a-z]+.'.$url.'/', $link) > 0 ) return 'internal';
            else return 'external';
            
    }

}
