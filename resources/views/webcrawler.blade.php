<!DOCTYPE html>
<head>
    <meta charset="utf-8">
<title>WEB CRAWLER - MANUEL MUJICA POCAY</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <!-- TOP CONTAINERS -->
    <div class="row m-3">
        <div class="card m-1 bg-dark col-sm-5 col-md-2">
        <h1 class="text-white m-3"><center>{{$rounds}}</center></h1>
        <div class="card-body">
            <h6 class="card-title text-white"><center>TOTAL PAGES CRAWLED</center></h6>
        </div>
        </div>
        <div class="card m-1 bg-dark col-sm-5 col-md-2">
        <h1 class="text-white m-3"><center>{{$average['load_time']}}s</center></h1>
        <div class="card-body">
            <h6 class="card-title text-white"><center>AVERAGE LOAD IN SECONDS</center></h6>
        </div>
        </div>
        <div class="card m-1 bg-dark col-sm-5 col-md-2">
        <h1 class="text-white m-3"><center>{{$average['word_count']}}</center></h1>
        <div class="card-body">
            <h6 class="card-title text-white"><center>AVERAGE WORD COUNT</center></h6>
        </div>
        </div>
        <div class="card m-1 bg-dark col-sm-5 col-md-2">
        <h1 class="text-white m-3"><center>{{$average['title_length']}}</center></h1>
        <div class="card-body">
            <h6 class="card-title text-white"><center>AVERAGE TITLE LENGHT</center></h6>
        </div>
        </div>
</div>
<!-- TOP BUTTONS -->
<div class="row m-3">
        <div class="card m-1 bg-light col-sm-5 col-md-2">
        <div class="card-body">
            <button type="button" class="btn btn-sm btn-secondary position-relative">
            TOTAL UNIQUE INTERNAL LINKS
            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                {{$total_link_qty['internal'] ?? ''}}
                <span class="visually-hidden">This value doesn't count the unique links repeated between different pages</span>
            </span>
            </button>
        </div>
        </div>
        <div class="card m-1 bg-light col-sm-5 col-md-2">
        <div class="card-body">
            <button type="button" class="btn btn-sm btn-secondary position-relative">
            TOTAL UNIQUE EXTERNAL LINKS
            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
            {{$total_link_qty['external'] ?? ''}}
                <span class="visually-hidden">This value doesn't count the unique links repeated between different pages</span>
            </span>
            </button>
        </div>
        </div>
        <div class="card m-1 bg-light col-sm-5 col-md-2">
        <div class="card-body">
            <button type="button" class="btn btn-sm btn-primary position-relative" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
            TOTAL UNIQUE IMAGES CLICK HERE
            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                {{count($img_array) ?? ''}}
                <span class="visually-hidden">This value doesn't count the unique links repeated between different pages</span>
            </span>
            </button>
        </div>
        </div>
        <div class="card m-1 bg-light col-sm-5 col-md-2">
        <div class="card-body">
            <button type="button" class="btn btn-sm btn-dark position-relative" data-bs-toggle="modal" data-bs-target="#exampleModal"">
            TRY A DIFFERENT CRAWL HERE
             </button>
        </div>
        </div>
</div>
<!-- MAIN CARD -->
<div class="card m-3">
    <div class="card-body">
    
<div class="d-flex align-items-start">
  <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">

@foreach ($page_crawled_list as $page)
    @foreach ($page as $info)  
    <button class="nav-link  @if($loop->parent->first) active @endif" id="v-pills-{{$loop->parent->index}}-tab" data-bs-toggle="pill" data-bs-target="#v-pills-{{$loop->parent->index}}" type="button" role="tab" aria-controls="v-pills-{{$loop->parent->index}}" aria-selected="false">{{$info['title'] ?? 'no-title'}}</button>
    @endforeach
@endforeach
  </div>
  <div class="tab-content" id="v-pills-tabContent">
  
  @foreach ($page_crawled_list as $page)
    @foreach ($page as $info)        
    <div class="tab-pane fade @if($loop->parent->first) active show @endif" id="v-pills-{{$loop->parent->index}}" role="tabpanel" aria-labelledby="v-pills-{{$loop->parent->index}}-tab">
        
    <ul class="list-group" style="width: 50rem;">
    <li class="list-group-item d-flex justify-content-between align-items-center"><strong>Title:</strong> {{$info['title'] ?? 'no-title'}}</li>
    <li class="list-group-item d-flex justify-content-between align-items-center"><strong>Title Length:</strong><span class="badge bg-secondary rounded-pill">{{strlen(($info['title'] ?? ''))}}</span></li>
    <li class="list-group-item d-flex justify-content-between align-items-center"><strong>URL: </strong>{{($info['url'] ?? 'no-url')}}</li>
    <li class="list-group-item d-flex justify-content-between align-items-center"><strong>Reponse: </strong><span class="badge bg-secondary rounded-pill">{{($info['response'] ?? 'error')}}</span></li>
    <li class="list-group-item d-flex justify-content-between align-items-center"><strong>Load Time: </strong><span class="badge bg-secondary rounded-pill">{{$info['load_time'] ?? 0.00}} seconds</span></li>
    
    <ul class="nav nav-tabs mt-3 mb-3" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="int-links-tab-{{$loop->parent->index}}" data-bs-toggle="tab" data-bs-target="#int-links-{{$loop->parent->index}}" type="button" role="tab" aria-controls="int-links-{{$loop->parent->index}}" aria-selected="true"><strong> Unique Internal links</strong> <span class="badge bg-secondary rounded-pill">{{($info['internal_link_qty'] ?? 0)}}</span></button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="ext-links-tab-{{$loop->parent->index}}" data-bs-toggle="tab" data-bs-target="#ext-links-{{$loop->parent->index}}" type="button" role="tab" aria-controls="ext-links-{{$loop->parent->index}}" aria-selected="true"><strong> Unique External links</strong> <span class="badge bg-secondary rounded-pill">{{($info['external_link_qty'] ?? 0)}}</span></button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="words-tab-{{$loop->parent->index}}" data-bs-toggle="tab" data-bs-target="#words-{{$loop->parent->index}}" type="button" role="tab" aria-controls="words-{{$loop->parent->index}}" aria-selected="false"><strong> Word Count: </strong><span class="badge bg-secondary rounded-pill">{{($info['word_count'] ?? 0)}}</span></button>
    </li>
    </ul>
    </ul>
    <div class="tab-content" id="myTabContent">
  <div class="tab-pane fade" id="words-{{$loop->parent->index}}" role="tabpanel" aria-labelledby="words-tab-{{$loop->parent->index}}">
    @if (isset($info['words']))
    @foreach ($info['words'] as $word)
    <button type="button" class="btn btn-sm btn-light m-1">
    <span class="badge bg-light text-dark">{{$word}} </span>
    </button>
    @if (($loop->index+1)%4 == 0)
        </br>
    @endif
    @endforeach
    @endif
  </div>
  <div class="tab-pane fade  show active" id="int-links-{{$loop->parent->index}}" role="tabpanel" aria-labelledby="int-links-tab">
    @if (isset($info['links']))
    @php($key=0)
    @foreach ($info['links'] as $link)
       @if ($link['type'] =='internal')  {{++ $key}} - {{$link['link']}} </span><br>@endif
    @endforeach
    @endif
  </div>
  <div class="tab-pane fade" id="ext-links-{{$loop->parent->index}}" role="tabpanel" aria-labelledby="ext-links-tab">
    @if (isset($info['links']))
    @php($key=0)
    @foreach ( $info['links'] as $link)
       @if ($link['type'] =='external') {{++ $key}} - {{$link['link']}} </span><br>@endif
    @endforeach
    @endif
  </div>
</div>

    </div>

    @endforeach
@endforeach
  </div>
</div>
    

    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
<!-- MODAL -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-fullscreen">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">IMAGES</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      @foreach ($img_array as $img)
            <div class="row">
            <div class="col-3">
            @if (strpos($img,'http') > -1)
            <img class="border" src="{{$img}}" height="70px" alt="{{$img}}" title="{{$img}}"> 
            @else
            <img class="border" src="{{$base_url.$img}}" height="70px" alt="{{$img}}" title="{{$img}}" >
            @endif
            </div>
            <div class="col-9">
            <span class="fw-lighter">{{$img}}</span>
            </div>
          </div>
          @endforeach
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Understood</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">TRY ANOTHER CRAWL</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">

      <select id="protocol" class="form-select mb-3" size="3"  aria-label="multiple select example">
        <option value="http">http</option>
        <option value="https" selected>https</option>
      </select>

      <div class="mb-3 ">
        <label for="inputUrl" class="form-label">Domain (without http / https)</label>
        <input type="text" class="form-control" id="domain" aria-describedby="inputUrl" placeholder="agencyanalytics.com" value="agencyanalytics.com">
      </div>
      
      <div class="mb-3">
        <label for="inputQty" class="form-label">Pages to Crawl</label>
        <input type="text" class="form-control" id="qty" placeholder="6" value="6">
      </div>
      

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="go();false;">Proceed</button>
      </div>
    </div>
  </div>
</div>
<script>
function go (){
  if (document.getElementById("protocol").value  != "" && document.getElementById("qty").value != ""){
    window.location.href = '/crawl/' + document.getElementById("protocol").value + '/' +document.getElementById("domain").value+ '/' + document.getElementById("qty").value;
  }
}
</script>